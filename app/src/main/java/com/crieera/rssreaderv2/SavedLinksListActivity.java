package com.crieera.rssreaderv2;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class SavedLinksListActivity extends Activity {
//    SQLiteDatabase db;
    MyDatabaseAdapter dbAdaptor;
    ListView savedListView;

//    db = new DBAdapter(this);
//    SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
//            android.R.layout.simple_list_item_1,
//            db.getAllTitles(),
//            new String[] { "title" },
//            new int[] { android.R.id.text1 });
//



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.saved_links_list);
        savedListView = (ListView) findViewById(R.id.saved_links_view);
//        try {
            dbAdaptor = new MyDatabaseAdapter(this);
//            db.open();
//            SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
//            R.layout.rss_item,
//            db.getAllDataTilesWithCursorMethod(),
//            new String[] { "title" },
//            new int[] { R.id.itemTitle });
//
//            savedListView = (ListView) findViewById(R.id.saved_links_view);
//            savedListView.setAdapter(adapter);
//
//        } catch (Exception e) {
//            Toast toast = Toast.makeText(this,
//                    "Error showing saved item list, see logs", Toast.LENGTH_LONG);
//            toast.show();
//        }

//            db = openOrCreateDatabase(
//                    "SavedLinksDB.db"
//                    , SQLiteDatabase.CREATE_IF_NECESSARY
//                    , null
//            );
//            dbAdaptor.open();
//            SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
//                    R.layout.rss_item,
//                    dbAdaptor.getAllDataTilesWithCursorMethod(),
//                    new String[] { "title" },
//                    new int[] { R.id.itemTitle });
//
//            savedListView = (ListView) findViewById(R.id.saved_links_view);
//            savedListView.setAdapter(adapter);


        List<RssItem> savedItemsFromDB = dbAdaptor.getAllDatasInArray();
        List<String> stringResult = new ArrayList<>(savedItemsFromDB.size());
        for (RssItem item:savedItemsFromDB) {
            stringResult.add(item.getTitle());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.rss_item, stringResult);
        savedListView.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
//        db = openOrCreateDatabase(
//                "SavedLinksDB.db"
//                , SQLiteDatabase.CREATE_IF_NECESSARY
//                , null
//        );


//        dbAdaptor.open();
//        Cursor dataFromDbCursor = dbAdaptor.getAllDataWithCursorMethod();
//        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
//                R.layout.rss_item,
//                dataFromDbCursor,
//                new String[] {},
//                new int[] { R.id.itemTitle });
//        savedListView = (ListView) findViewById(R.id.saved_links_view);
//        savedListView.setAdapter(adapter);

    }
}
