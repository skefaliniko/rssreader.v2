package com.crieera.rssreaderv2;

public class RssItem {

	private String title;
	private String link;

	public RssItem(String title, String link) {
		this.title = title;
		this.link = link;
	}

	public RssItem() {

	}

	public String getTitle() {
		return title;
	}

	public String getLink() {
		return link;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
