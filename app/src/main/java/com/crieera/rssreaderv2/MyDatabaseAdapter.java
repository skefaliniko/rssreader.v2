package com.crieera.rssreaderv2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MyDatabaseAdapter {

    public static final String KEY_ROWID = "_id";
    public static final String KEY_TITLE = "NewsTitle";
    public static final String KEY_LINK = "NewsLink";
    private static final String TAG = "MyDatabaseAdapter";
    private static final String DATABASE_NAME = "SavedLinksDB2.db";
    private static final String DATABASE_TABLE = "Saved_links_table";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE = "create table " + DATABASE_TABLE +
            " (" + KEY_ROWID + " integer primary key autoincrement, "
            + KEY_TITLE + " text not null, " + KEY_LINK + " text not null);";

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public MyDatabaseAdapter(Context ctx)    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context)     {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)   {
            try  {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)   {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }
    }

    //---opens the database---
    public MyDatabaseAdapter open() throws SQLException      {

        db = DBHelper.getWritableDatabase();
        return this;
    }


    //---closes the database---
    public void close()   {
        DBHelper.close();
    }

    //---insert a contact into the database---
    public long insertContact(String title, String link)   {

        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_TITLE, title);
        initialValues.put(KEY_LINK, link);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    //---deletes a particular contact---
    public boolean deleteContact(long rowId)    {

        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    public List<RssItem> getAllDatasInArray() {
        List<RssItem> dataList = new ArrayList<RssItem>();
        // Select All Query
//        String selectQuery = "SELECT " + KEY_TITLE + " FROM " + DATABASE_TABLE;
        String selectQuery = "SELECT * FROM " + DATABASE_TABLE;


        db = DBHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RssItem data = new RssItem(cursor.getString(1), cursor.getString(2));//0 for id
                dataList.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return dataList;
    }

    public Cursor getContactsInCursor(long rowId) throws SQLException {

        SQLiteDatabase db = DBHelper.getWritableDatabase();
        String query = "SELECT * " + KEY_TITLE + "FROM " + DATABASE_TABLE;
        Cursor data = db.rawQuery(query, null);
        return data;

        //wrong?
//        Cursor mCursor = db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TITLE, KEY_LINK},
//                KEY_ROWID + "=" + rowId, null,null, null, null, null);
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//        }
//        return mCursor;
    }

    //---updates a contact---
    public boolean updateContact(long rowId, String title, String link)   {

        ContentValues args = new ContentValues();
        args.put(KEY_TITLE, title);
        args.put(KEY_LINK, link);
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

//    public Cursor SortAllRows() {
//        return db.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TITLE, KEY_LINK}
//                , null, null, null, null, null);
//    }

}