package com.crieera.rssreaderv2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity{
    Button getNewsFromNetButton;
    Button viewSavedLinksButton;
    Button stopServiceButton;
    Button startServiceAgainButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startService(new Intent(this, RssService.class));

        Toast toast = Toast.makeText(getApplicationContext(),
                "Service started!", Toast.LENGTH_SHORT);
        toast.show();

        getNewsFromNetButton = (Button) findViewById(R.id.get_news_button);
        viewSavedLinksButton = (Button) findViewById(R.id.view_saved_links_button);
        stopServiceButton = (Button) findViewById(R.id.stop_service_button);
        startServiceAgainButton = (Button) findViewById(R.id.start_service_again_button);

        getNewsFromNetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToNewsIntent = new Intent(MainActivity.this, MyListActivity.class);
                startActivity(goToNewsIntent);
            }
        });

        viewSavedLinksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToNewsIntent = new Intent(MainActivity.this, SavedLinksListActivity.class);
                startActivity(goToNewsIntent);
            }
        });

        stopServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Service stopped by clicking!", Toast.LENGTH_LONG);
                toast.show();

                stopService(new Intent(MainActivity.this, RssService.class));
            }
        });

        startServiceAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Service started by clicking!", Toast.LENGTH_LONG);
                toast.show();

                stopService(new Intent(MainActivity.this, RssService.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Toast toast = Toast.makeText(getApplicationContext(),
                "Service stopped onDestroy!", Toast.LENGTH_LONG);
        toast.show();

        stopService(new Intent(this, RssService.class));
    }

}
